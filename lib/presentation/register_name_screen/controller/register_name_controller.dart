import 'package:flutter/material.dart';
import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/register_name_screen/models/register_name_model.dart';

class RegisterNameController extends GetxController {
  Rx<RegisterNameModel> registerNameModelObj = RegisterNameModel().obs;
  TextEditingController nameController = TextEditingController();
  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
