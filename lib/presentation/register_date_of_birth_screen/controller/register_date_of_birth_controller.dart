import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/register_date_of_birth_screen/models/register_date_of_birth_model.dart';

class RegisterDateOfBirthController extends GetxController {
  Rx<RegisterDateOfBirthModel> registerDateOfBirthModelObj =
      RegisterDateOfBirthModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
