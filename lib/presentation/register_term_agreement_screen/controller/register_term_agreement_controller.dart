import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/register_term_agreement_screen/models/register_term_agreement_model.dart';

class RegisterTermAgreementController extends GetxController {
  Rx<RegisterTermAgreementModel> registerTermAgreementModelObj =
      RegisterTermAgreementModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
