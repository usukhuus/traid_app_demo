import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/withdraw_confirm_bank_account_one_screen/models/withdraw_confirm_bank_account_one_model.dart';

class WithdrawConfirmBankAccountOneController extends GetxController {
  Rx<WithdrawConfirmBankAccountOneModel> withdrawConfirmBankAccountOneModelObj =
      WithdrawConfirmBankAccountOneModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
