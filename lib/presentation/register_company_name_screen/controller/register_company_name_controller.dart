import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/register_company_name_screen/models/register_company_name_model.dart';

class RegisterCompanyNameController extends GetxController {
  Rx<RegisterCompanyNameModel> registerCompanyNameModelObj =
      RegisterCompanyNameModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
