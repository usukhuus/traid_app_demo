import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/register_job_screen/models/register_job_model.dart';

class RegisterJobController extends GetxController {
  Rx<RegisterJobModel> registerJobModelObj = RegisterJobModel().obs;

  RxString radioGroup = "".obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
