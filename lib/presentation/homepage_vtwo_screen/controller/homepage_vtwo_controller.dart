import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/homepage_vtwo_screen/models/homepage_vtwo_model.dart';

class HomepageVtwoController extends GetxController {
  Rx<HomepageVtwoModel> homepageVtwoModelObj = HomepageVtwoModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
