import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/register_selfie_w_id_card_one_screen/models/register_selfie_w_id_card_one_model.dart';

class RegisterSelfieWIdCardOneController extends GetxController {
  Rx<RegisterSelfieWIdCardOneModel> registerSelfieWIdCardOneModelObj =
      RegisterSelfieWIdCardOneModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
