import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/success_bottomsheet/models/success_model.dart';

class SuccessController extends GetxController {
  Rx<SuccessModel> successModelObj = SuccessModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
