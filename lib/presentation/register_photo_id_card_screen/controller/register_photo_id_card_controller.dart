import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/register_photo_id_card_screen/models/register_photo_id_card_model.dart';

class RegisterPhotoIdCardController extends GetxController {
  Rx<RegisterPhotoIdCardModel> registerPhotoIdCardModelObj =
      RegisterPhotoIdCardModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
