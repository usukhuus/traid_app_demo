import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/history_activities_page/models/history_activities_model.dart';

class HistoryActivitiesController extends GetxController {
  HistoryActivitiesController(this.historyActivitiesModelObj);

  Rx<HistoryActivitiesModel> historyActivitiesModelObj;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
