import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/withdraw_success_bottomsheet/models/withdraw_success_model.dart';

class WithdrawSuccessController extends GetxController {
  Rx<WithdrawSuccessModel> withdrawSuccessModelObj = WithdrawSuccessModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
