import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/success_two_bottomsheet/models/success_two_model.dart';

class SuccessTwoController extends GetxController {
  Rx<SuccessTwoModel> successTwoModelObj = SuccessTwoModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
