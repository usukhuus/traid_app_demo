import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/deposit_select_payment_screen/models/deposit_select_payment_model.dart';

class DepositSelectPaymentController extends GetxController {
  Rx<DepositSelectPaymentModel> depositSelectPaymentModelObj =
      DepositSelectPaymentModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
