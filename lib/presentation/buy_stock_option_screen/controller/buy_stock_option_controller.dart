import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/buy_stock_option_screen/models/buy_stock_option_model.dart';

class BuyStockOptionController extends GetxController {
  Rx<BuyStockOptionModel> buyStockOptionModelObj = BuyStockOptionModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
