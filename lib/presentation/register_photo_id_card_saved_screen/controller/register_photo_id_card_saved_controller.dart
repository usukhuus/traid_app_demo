import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/register_photo_id_card_saved_screen/models/register_photo_id_card_saved_model.dart';

class RegisterPhotoIdCardSavedController extends GetxController {
  Rx<RegisterPhotoIdCardSavedModel> registerPhotoIdCardSavedModelObj =
      RegisterPhotoIdCardSavedModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
