import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/register_selfie_w_id_card_screen/models/register_selfie_w_id_card_model.dart';

class RegisterSelfieWIdCardController extends GetxController {
  Rx<RegisterSelfieWIdCardModel> registerSelfieWIdCardModelObj =
      RegisterSelfieWIdCardModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
