import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/register_hope_to_achieve_screen/models/register_hope_to_achieve_model.dart';

class RegisterHopeToAchieveController extends GetxController {
  Rx<RegisterHopeToAchieveModel> registerHopeToAchieveModelObj =
      RegisterHopeToAchieveModel().obs;

  RxString radioGroup = "".obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
