import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/withdraw_summary_screen/models/withdraw_summary_model.dart';

class WithdrawSummaryController extends GetxController {
  Rx<WithdrawSummaryModel> withdrawSummaryModelObj = WithdrawSummaryModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
