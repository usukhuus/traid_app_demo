import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/success_one_bottomsheet/models/success_one_model.dart';

class SuccessOneController extends GetxController {
  Rx<SuccessOneModel> successOneModelObj = SuccessOneModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
