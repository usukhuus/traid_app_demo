import 'package:flutter/cupertino.dart';
import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/register_set_pin_screen/models/register_set_pin_model.dart';

class RegisterSetPinController extends GetxController {
  Rx<RegisterSetPinModel> registerSetPinModelObj = RegisterSetPinModel().obs;
  TextEditingController otpController = TextEditingController();

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
