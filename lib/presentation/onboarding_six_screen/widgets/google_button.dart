import 'package:flutter/material.dart';
import 'package:traidy/core/app_export.dart';

import '../controller/onboarding_six_controller.dart';

class GoogleButton extends GetView<OnboardingSixController> {
  const GoogleButton({super.key});

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: () {},
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/onboarding/ic_google_logo.png',
            width: getHorizontalSize(24),
            height: getVerticalSize(24),
          ),
          SizedBox(
            width: getHorizontalSize(8),
          ),
          Text(
            'lbl_continue_with_google'.tr,
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: Colors.black,
              fontSize: 16,
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
    return Container(
      padding: EdgeInsets.symmetric(horizontal: getHorizontalSize(26), vertical: getVerticalSize(12)),
      decoration: ShapeDecoration(
        shape: RoundedRectangleBorder(
          side: const BorderSide(width: 1, color: Color(0xFFD9D9D9)),
          borderRadius: BorderRadius.circular(28),
        ),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/onboarding/ic_google_logo.png',
            width: getHorizontalSize(24),
            height: getVerticalSize(24),
          ),
          SizedBox(
            width: getHorizontalSize(8),
          ),
          Text(
            'lbl_continue_with_google'.tr,
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: Colors.black,
              fontSize: 16,
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
  }
}
