import 'package:flutter/material.dart';
import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/onboarding_six_screen/controller/onboarding_six_controller.dart';

class OnboardingItem extends GetView<OnboardingSixController> {
  final dynamic data;
  const OnboardingItem({required this.data, super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: getVerticalSize(32),
        ),
        Expanded(
          child: Stack(
            children: [
              Image.asset(
                'assets/images/onboarding/onboarding_image_1.png',
                width: double.infinity,
                height: double.infinity,
                fit: BoxFit.cover,
              ),
              Container(
                width: double.infinity,
                height: double.infinity,
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment(0.00, -1.00),
                    end: Alignment(0, 1),
                    colors: [Color(0x00D9D9D9), Colors.white],
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: getVerticalSize(32),
        ),
        Text.rich(
          style: const TextStyle(
            color: Colors.black,
            fontSize: 28,
            fontWeight: FontWeight.w700,
          ),
          TextSpan(
            children: [
              const TextSpan(
                text: 'Welcome to ',
              ),
              TextSpan(
                text: 'Traidy 👋',
                style: TextStyle(
                  color: Get.theme.colorScheme.primary,
                ),
              ),
            ],
          ),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: getVerticalSize(16),
        ),
        const Text(
          'The best app to invest in international stocks   with as little as \$1,00',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.black,
            fontSize: 16,
            fontWeight: FontWeight.w600,
          ),
        ),
      ],
    );
  }
}
