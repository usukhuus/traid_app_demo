import 'package:flutter/material.dart';
import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/onboarding_six_screen/widgets/onboarding_item.dart';

import 'controller/onboarding_six_controller.dart';
import 'models/onboarding_six_model.dart';
import 'models/sliderwelcometoinvest_one_item_model.dart';
import 'widgets/google_button.dart';

// ignore: must_be_immutable
class OnboardingScreen extends GetWidget<OnboardingSixController> {
  final PageController pageController = PageController();

  List<SliderwelcometoinvestOneItemModel> sliderPage = OnboardingSixModel.getSliderPageData();

  OnboardingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorConstant.whiteA700,
      body: GetBuilder<OnboardingSixController>(
        init: OnboardingSixController(),
        builder: (onboardingSixController) => SafeArea(
          child: Column(
            children: [
              Expanded(
                child: PageView.builder(
                  onPageChanged: (value) {
                    onboardingSixController.setIndex(value);
                  },
                  controller: pageController,
                  scrollDirection: Axis.horizontal,
                  itemCount: sliderPage.length,
                  itemBuilder: (context, index) {
                    return OnboardingItem(
                      data: sliderPage[index],
                    );
                  },
                ),
              ),
              SizedBox(
                height: getVerticalSize(24),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    children: [
                      ///Page indicator
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: List.generate(
                          sliderPage.length,
                          (index) {
                            return AnimatedContainer(
                              duration: const Duration(milliseconds: 300),
                              height: getVerticalSize(6),
                              width: getHorizontalSize(onboardingSixController.currentPage == index ? 24 : 6),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6),
                                color: (index == onboardingSixController.currentPage) ? Get.theme.colorScheme.primary : ColorConstant.gray200,
                              ),
                            ).paddingSymmetric(
                              horizontal: getHorizontalSize(3),
                            );
                          },
                        ),
                      ),
                      SizedBox(height: getVerticalSize(32)),
                      const GoogleButton(),
                      SizedBox(
                        height: getVerticalSize(12),
                      ),
                      ElevatedButton(
                        onPressed: () {
                          Get.toNamed(AppRoutes.registerNameScreen);
                        },
                        child: Text('lbl_register'.tr),
                      ),
                      SizedBox(
                        height: getVerticalSize(12),
                      ),
                      ElevatedButton(
                        onPressed: () {
                          Get.toNamed(AppRoutes.loginScreen);
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Get.theme.primaryColorLight,
                          foregroundColor: Get.theme.colorScheme.primary,
                        ),
                        child: Text('lbl_login'.tr),
                      ),
                      SizedBox(
                        height: getVerticalSize(32),
                      )
                    ],
                  ),
                ],
              ).paddingSymmetric(
                horizontal: getHorizontalSize(24),
              ),
            ],
          ),
        ),
      ),
    );
  }

  onTapGoogle() async {}
}
