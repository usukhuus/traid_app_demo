import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/withdraw_add_bank_account_screen/models/withdraw_add_bank_account_model.dart';
import 'package:flutter/material.dart';

class WithdrawAddBankAccountController extends GetxController {
  TextEditingController searchboxController = TextEditingController();

  Rx<WithdrawAddBankAccountModel> withdrawAddBankAccountModelObj =
      WithdrawAddBankAccountModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
    searchboxController.dispose();
  }
}
