import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/summary_two_screen/models/summary_two_model.dart';

class SummaryTwoController extends GetxController {
  Rx<SummaryTwoModel> summaryTwoModelObj = SummaryTwoModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
