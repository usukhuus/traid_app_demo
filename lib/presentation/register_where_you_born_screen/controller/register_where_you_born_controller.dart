import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/register_where_you_born_screen/models/register_where_you_born_model.dart';

class RegisterWhereYouBornController extends GetxController {
  Rx<RegisterWhereYouBornModel> registerWhereYouBornModelObj =
      RegisterWhereYouBornModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
