import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/withdraw_confirm_bank_account_screen/models/withdraw_confirm_bank_account_model.dart';

class WithdrawConfirmBankAccountController extends GetxController {
  Rx<WithdrawConfirmBankAccountModel> withdrawConfirmBankAccountModelObj =
      WithdrawConfirmBankAccountModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
