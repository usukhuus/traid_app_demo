import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/register_upload_or_take_id_card_screen/models/register_upload_or_take_id_card_model.dart';

class RegisterUploadOrTakeIdCardController extends GetxController {
  Rx<RegisterUploadOrTakeIdCardModel> registerUploadOrTakeIdCardModelObj =
      RegisterUploadOrTakeIdCardModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
