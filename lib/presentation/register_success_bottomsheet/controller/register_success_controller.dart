import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/register_success_bottomsheet/models/register_success_model.dart';

class RegisterSuccessController extends GetxController {
  Rx<RegisterSuccessModel> registerSuccessModelObj = RegisterSuccessModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
