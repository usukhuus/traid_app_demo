import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/sbux_stock_share_bottomsheet/models/sbux_stock_share_model.dart';

class SbuxStockShareController extends GetxController {
  Rx<SbuxStockShareModel> sbuxStockShareModelObj = SbuxStockShareModel().obs;

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
