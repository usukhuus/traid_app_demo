import 'dart:async';

import 'package:traidy/core/app_export.dart';
import 'package:traidy/presentation/splash_screen/models/splash_model.dart';

class SplashController extends GetxController {
  Rx<SplashModel> splashModelObj = SplashModel().obs;
  Rx<SplashModel> newSplashObj = SplashModel().obs;
  
  @override
  Future<void> onReady() async {
    bool isLogin = await PrefUtils.getIsLogin();
    super.onReady();
    Timer(const Duration(seconds: 3), () {
      if (isLogin) {
        Get.toNamed(AppRoutes.onboardingSixScreen);
      } else {
        Get.toNamed(AppRoutes.homepageVthreeContainerScreen);
      }
    });
  }

  @override
  void onClose() {
    super.onClose();
  }
}
