import 'package:flutter/material.dart';
import 'package:traidy/core/app_export.dart';

import 'controller/splash_controller.dart';

class SplashScreen extends GetWidget<SplashController> {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorConstant.whiteA700,
      body: SafeArea(
        child: SizedBox(
          width: size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomImageView(
                svgPath: ImageConstant.imgProfits1,
                height: getSize(200.00),
                width: getSize(200.00),
              ),
              Padding(
                padding: getPadding(top: 19, bottom: 7),
                child: Text(
                  "lbl_invest".tr,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.left,
                  style: AppStyle.txtInterThin32.copyWith(
                    letterSpacing: getHorizontalSize(0.40),
                    height: getVerticalSize(1.01),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
