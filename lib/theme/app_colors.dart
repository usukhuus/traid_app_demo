import 'package:flutter/material.dart';

class AppColors {
  static const Color primary = Color(0xFF12D18E);
  static const Color primaryLight = Color(0xFFE7FAF4);
  static const Color inactive = Color(0xFFEFEFEF);
}
